<?php
//namespace Tests;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()	{
	    $provider = new CarrierInterface();
		$mobile = new Mobile($provider);
		$this->assertNull(null,$mobile->makeCallByName(''));
	}

	public function it_returns_null_when_contact_not_exist()	{	   
		$contact = new Contact();
		$this->assertNull(null,$contact->FindContactName(''));
	} 

	public function send_sms()	{
		$number='956734564';
		$body="Hello, how are you?";
	   
		$provider = new CarrierInterface();
		$contact_service = new ContactService();
        $message = new SMS();
		$rs_number = $contact_service->validateNumber($number);
		if($rs_number==true){
			$this->assertNull('enviado',$message->SendSms($number,$body));
		}
	} 
}
