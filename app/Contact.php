<?php

namespace App;
use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
class Contact
{
	
	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}
	
	public function FindContactName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);
         
		if(isset($contact)){
			$this->provider->dialContact($contact);
			$this->provider->makeCall();
			return "calling";
		}else{
            return null;
		}

	}

}