<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName(): Contact
	{
		// queries to the db
		$sql="select id, name from contact where name =".$name;
		$rs = mysql_query($sql);
		return $rs;
	}

	public static function validateNumber(string $number, int $minDigits = 9, int $maxDigits = 14): bool
	{
		if (preg_match('/^[+][0-9]/', $number)) { 
			$count = 1;
			$telephone = str_replace(['+'], '', $number, $count); 
		}		
	
		$number = str_replace([' ', '.', '-', '(', ')'], '', $number); 	
		
		return isDigits($number, $minDigits, $maxDigits); 
	}

	function isDigits(string $s, int $minDigits = 9, int $maxDigits = 14): bool {
		return preg_match('/^[0-9]{'.$minDigits.','.$maxDigits.'}\z/', $s);
	}
}